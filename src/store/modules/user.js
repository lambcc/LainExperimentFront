export default {
  namespaced: true,
  state: {
    id: 0,
    name: '',
    token: ''
  },
  mutations: {
    updateId (state, id) {
      state.id = id
    },
    updateName (state, name) {
      state.name = name
    },
    updateToken(state, token) {
      state.token = token
    }
  }
}
